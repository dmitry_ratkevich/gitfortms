var = (lambda **kwargs: {key * 2: value for key, value in kwargs.items()})
print(var(abc=5))
