def decor(f):
    def wrapper(n):
        filter_list = list(filter(lambda x: x % 2 != 0, n))
        return f(filter_list)
    return wrapper


@decor
def func(lst):
    return print(lst)


lst = [i for i in range(1, 11)]
func(lst)
