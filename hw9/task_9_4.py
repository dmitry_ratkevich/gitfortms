from functools import wraps as w


def roller(func):
    @w(func)
    def new(*args):
        return func(*args[::-1])
    return new

@roller
def super_function(a, b, c):
    return print("GIVE ME MORE POWER!", a + b / c)


super_function(0.1, 1, 0)
