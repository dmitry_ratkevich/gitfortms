dict_a = {'test': 'test_value',
          'europe': 'eur',
          'dollar': 'usd',
          'ruble': 'rub'
          }
replacements = {}
for key, value in dict_a.items():
    kl = len(key)
    k = key + str(kl)
    replacements[k] = dict_a[key]
print(replacements)


keys = list(dict_a.keys())
replacements = {}
i = 0
while i < len(keys):
    key = keys[i]
    kl = len(key)
    k = f'{key}{kl}'
    replacements[k] = dict_a[key]
    i += 1
print(replacements)
