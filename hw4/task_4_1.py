# Дан список целых чисел.
# Создать новый список, каждый элемент которого равен исходному элементу
# умноженному на -2


spis_a = [1, 2, 3, 4, 5, 5, -7]
spis_b = []
for i in spis_a:
    spis_b.append(i * -2)
print(f'with cycle for: {spis_b}')


spis_c = [-1, -2, -3, -4, -5, -5, 7]
lens = len(spis_c)
spis_d = []
x = 0
while x < lens:
    i = spis_c[-1]
    spis_d.append(i * -2)
    spis_c.pop()
    x += 1
spis_d.reverse()
print(f'with cycle while: {spis_d}')