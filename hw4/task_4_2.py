# Дан список целых чисел. Подсчитать сколько четных чисел в списке


from random import randint


lst = [randint(1, 99) for i in range(7)]
print(f'For random list: {lst}')
count = 0
for i in lst:
    if i % 2 == 0:
        count += 1
print(f'With cycle for - even numbers in list: {count}')


count_w = 0
i = 0
while i < len(lst):
    if lst[i] % 2 == 0:
        count_w += 1
    i += 1
print(f'With cycle while - even numbers in list: {count_w}')
