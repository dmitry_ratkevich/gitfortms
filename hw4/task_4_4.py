print('For method "For":')
a = list(range(1, 7))
b = []
i = a[0]
for i in a:
    b.insert(len(b) - 1, i)
print(f'Not update list: {a}')
print(f'Updated list: {b}')
print('\n_______\n')
print('For method "While":')
a = list(range(1, 7))
b = []
i = a[0]
while i <= len(a):
    b.insert(len(b) - 1, i)
    i += 1
print(f'Not update list: {a}')
print(f'Updated list: {b}')
