def addition(arg1, arg2):
    # arg1, arg2 = map(int, input("Enter a+b: ").split('+'))
    return f'Result addition = {arg1 + arg2}'


def subtraction(arg1, arg2):
    # arg1, arg2 = map(int, input("Enter 2 numbers separated by a space: ").split())
    return f'Result subtraction = {arg1 - arg2}'


def multiplication(arg1, arg2):
    # arg1, arg2 = map(int, input("Enter 2 numbers separated by a space: ").split())
    return f'Result multiplication = {arg1 * arg2}'


def division(arg1, arg2):
    # arg1, arg2 = map(int, input("Enter 2 numbers separated by a space: ").split())
    return f'Result division = {arg1 / arg2}'
