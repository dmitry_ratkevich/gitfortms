from typing import Set

from hw13_01.func import addition, subtraction, multiplication, division
from hw13_01.exceptions import MyError
import copy


def ui():
    while True:
        print("Enter a statement consisting of two variables, using the '+', '-', '*' or '/' symbol: ")
        user_input = input()
        validation_string = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '+', '-', '*', '/'}
        set_input = copy.copy(set(user_input))
        if set_input.issubset(validation_string):
                if '+' in user_input:
                    user_input = user_input.split('+')
                    arg1 = int(user_input[0])
                    arg2 = int(user_input[1])
                    print(addition(arg1, arg2))
                elif '-' in user_input:
                    user_input = user_input.split('-')
                    arg1 = int(user_input[0])
                    arg2 = int(user_input[1])
                    print(subtraction(arg1, arg2))
                elif '*' in user_input:
                    user_input = user_input.split('*')
                    arg1 = int(user_input[0])
                    arg2 = int(user_input[1])
                    print(multiplication(arg1, arg2))
                elif '/' in user_input:
                    try:
                        user_input = user_input.split('/')
                        arg1 = int(user_input[0])
                        arg2 = int(user_input[1])
                        print(division(arg1, arg2))
                    except ZeroDivisionError:
                        print('Division by zero!')
        else:
            print(MyError('Invalid literal input'))


ui()
