from random import randint

#                                                       task 1
matrix = []
for i in range(3):
    matrix.append([])
    for j in range(3):
        matrix[i].append(randint(1, 9))
print("Original:")
for row in matrix:
    print(*row)
#                                                       task 2
print("Maximal value in matrix:")
max_values = []
for row in matrix:
    max_values.append(max(row))
print(max(max_values))
#                                                       task 3
print("Minimal value in matrix:")
min_values = []
for row in matrix:
    min_values.append(min(row))
print(min(min_values))
#                                                       task 4
print("Sum all values in matrix:")
sum_values = []
for row in matrix:
    sum_values.append(sum(row))
print(sum(sum_values))
#                                                       task 5
print("Index of row of maximum value:")
index_max_row = sum_values.index(max(sum_values))
print(index_max_row)
#                                                       task 6
print("Index of column of maximum value:")
turn_matrix = list(zip(*matrix))
sum_turn_values = []
for row in turn_matrix:
    sum_turn_values.append(sum(row))
index_max_column = sum_turn_values.index(max(sum_turn_values))
print(index_max_column)
#                                                       task 7
print("Index of row of minimal value:")
index_min_row = sum_values.index(min(sum_values))
print(index_min_row)
#                                                       task 8
print("Index of column of minimal value:")
index_min_column = sum_turn_values.index(min(sum_turn_values))
print(index_min_column)
#                                                       task 9
print("Zeroing matrix above of main diagonal:")
for i, row in enumerate(matrix, start=1):
    for j in range(i, len(row)):
        row[j] = 0
[print(*row) for row in matrix]  # generator
#                                                       task 10
print("Zeroing matrix under of main diagonal:")
for i, row in enumerate(matrix):
    for j in range(i):
        row[j] = 0
[print(*row) for row in matrix]


#                                                       task 11
def matrix_creator(n, m):
    r = 10
    matrix = []
    for i in range(n):
        matrix.append([])
        for j in range(m):
            matrix[i].append(r)
            r += 1
    return matrix


matrix_a = matrix_creator(3, 3)
matrix_b = matrix_creator(3, 3)
print("Matrix A:")
[print(*row) for row in matrix_a]
print("Matrix B:")
[print(*row) for row in matrix_b]
#                                                       task 12
sum_matrix = matrix_creator(3, 3)
for i in range(len(matrix_a)):
    for j in range(len(matrix_a[0])):
        sum_matrix[i][j] = matrix_a[i][j] + matrix_b[i][j]
print("Sum matrix A and B:")
for a in sum_matrix:
    print(*a)
#                                                       task 13
diff_matrix = matrix_creator(3, 3)
print("Difference matrix A and B:")
for i in range(len(matrix_a)):
    for j in range(len(matrix_a[0])):
        diff_matrix[i][j] = matrix_a[i][j] - matrix_b[i][j]
for b in diff_matrix:
    print(*b)
#                                                       task 14
g = int(input("Enter integer multiplicator for matrix A: "))
mult_matrix = [[x * g for x in row] for row in matrix_a]
[print(*row) for row in mult_matrix]
