from datetime import datetime


trains = [
    {
        "number": "637B",
        "point_start": "Minsk",
        "date_start": datetime(2021, 1, 18, 20, 43),
        "point_end": "Baranovichi",
        "date_end": datetime(2021, 1, 18, 23, 37)
    },
    {
        "number": "420A",
        "point_start": "Minsk",
        "date_start": datetime(2021, 1, 18, 16, 30),
        "point_end": "Grodno",
        "date_end": datetime(2021, 1, 18, 22, 49)
    },
    {
        "number": "475G",
        "point_start": "Minsk",
        "date_start": datetime(2021, 1, 18, 14, 20),
        "point_end": "Gomel",
        "date_end": datetime(2021, 1, 18, 23, 7)
    }
        ]
for train in trains:
    time_delta = train['date_end'] - train['date_start']
    seconds_delta = time_delta.total_seconds()
    hours_delta = seconds_delta // (60 * 60)
    minutes_delta = (seconds_delta / 60) % 60
    if hours_delta > 7 and minutes_delta > 20:
        print(f'Train number: {train["number"]} (from {train["point_start"]} to {train["point_end"]}) - took time: '
              f'{hours_delta} hours {minutes_delta} minutes')
