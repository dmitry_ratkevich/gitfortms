# ""Дана целочисленная квадратная матрица. Найти в каждой строке наи-
# больший элемент и поменять его местами с элементом главной диагонали.""


from random import randint


n = 3
m = 3
matrix = []
for i in range(n):
    matrix.append([])
    for j in range(m):
        matrix[i].append(randint(1, 9))
print("Original matrix:\n")
for row in matrix:
    print(*row)
for y, x in enumerate(matrix):
    max_value = max(x)
    dig = matrix[y][y]
    max_index = x.index(max_value)
    matrix[y][y] = max_value
    matrix[y][max_index] = dig
print("Modified matrix:\n")
for row in matrix:
    print(*row)
