from random import randint


list_a = []
for i in range(19):
    list_a.append(randint(10, 100))
print(f'{list_a}\n--------')
count = 0
numbers_in_row = 0

for i, element in enumerate(list_a):
    if i + 1 < len(list_a):
        numbers_in_row += 1
        if element >= list_a[i + 1]:
            if numbers_in_row > 1:
                count += 1
            numbers_in_row = 0

print(count)
