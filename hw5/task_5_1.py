Z = None
print('(for break at program enter 0)')
while True:
    sign = input('enter the sign operation:\n')
    if sign == '0':
        break
    if sign in ('+', '-', '/', '*'):
        x = int(input('Enter the x: '))
        y = int(input('Enter the y: '))
        if sign == '+':
            z = x + y
            print(z)
        elif sign == '-':
            z = x - y
            print(z)
        elif sign == '/' and sign != 0:
            try:
                z = x / y
                print(z)
            except ZeroDivisionError:
                z = 0
                print('division at zero!', z)
        elif sign == '*':
            z = x * y
            print(z)
    else:
        print('invalid sign')
