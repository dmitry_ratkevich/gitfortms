from random import randint


n = []
for i in range(19):
    n.append(randint(10, 100))
print('original list:\n', n)
max_value = max(n)
print('maximum value:\n', max_value)
for x in range(len(n)):
    if n[x] % 2 == 0 and x != max_value:
        n[x] = max_value
print('altered list:\n', n)
