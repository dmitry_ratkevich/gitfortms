from faker import Faker
import csv
from random import randint

fake_ja = Faker()

data = [
    {'name': fake_ja.first_name(),
     'surname': fake_ja.last_name(),
     'age': randint(1, 105)} for i in range(50)
]

with open("task_10_1.csv", 'w') as file:
    writer = csv.DictWriter(file, fieldnames=data[0].keys())
    writer.writeheader()
    writer.writerows(data)

a = 0  # range 1 - 12
b = 0  # range 13 - 18
c = 0  # range 19 - 25
d = 0  # range 26 - 40
e = 0  # range 40+
with open("task_10_1.csv", 'r') as file:
    for row in csv.DictReader(file):
        age = int(row['age'])
        if 1 <= age <= 12:
            a += 1
        elif 13 <= age <= 18:
            b += 1
        elif 19 <= age <= 25:
            c += 1
        elif 26 <= age <= 40:
            d += 1
        else:
            e += 1
print(f"1-12 age - {a}\n13-18 age - {b}\n19-25 age - {c}\n26-40 age - {d}\n40+ age - {e}")
