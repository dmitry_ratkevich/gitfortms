def inch_to_cm():
    value = int(input("Input value: "))
    result = value * 2.54
    return print(f"In {value} inches - {round(result, 4)} centimeters")


def cm_to_inches():
    value = int(input("Input value: "))
    result = value / 2.54
    return print(f"In {value} centimeters - {round(result, 4)} inches")


def miles_to_kilometers():
    value = int(input("Input value: "))
    result = value / 1.6
    return print(f"In {value} miles - {round(result, 4)} kilometers")


def kilometers_to_miles():
    value = int(input("Input value: "))
    result = value * 1.6
    return print(f"In {value} kilometers - {round(result, 4)} miles")


def funds_to_kilograms():
    value = int(input("Input value: "))
    result = value * 0.454
    return print(f"In {value} funds - {round(result, 4)} kilograms")


def kilograms_to_funds():
    value = int(input("Input value: "))
    result = value * 2.205
    return print(f"In {value} kilograms - {round(result, 4)} funds")


def ounces_to_grams():
    value = int(input("Input value: "))
    result = value * 28.34952
    return print(f"In {value} ounces - {round(result, 4)} grams")


def grams_to_ounces():
    value = int(input("Input value: "))
    result = value / 28.34952
    return print(f"In {value} grams - {round(result, 4)} ounces")


def gallons_to_liters():
    value = int(input("Input value: "))
    result = value * 3.7854
    return print(f"In {value} gallons - {round(result, 4)} liters")


def liters_to_gallons():
    value = int(input("Input value: "))
    result = value / 3.7854
    return print(f"In {value} liters - {round(result, 4)} gallons")


def pints_to_liters():
    value = int(input("Input value: "))
    result = value * 0.4741
    return print(f"In {value} pints - {round(result, 4)} liters")


def liters_to_pints():
    value = float(input("Input value: "))
    result = value / 0.4741
    return print(f"In {value} liters - {round(result, 4)} pints")


directory_of_functions = {
    1: inch_to_cm,
    2: cm_to_inches,
    3: miles_to_kilometers,
    4: kilometers_to_miles,
    5: funds_to_kilograms,
    6: kilograms_to_funds,
    7: ounces_to_grams,
    8: grams_to_ounces,
    9: gallons_to_liters,
    10: liters_to_gallons,
    11: pints_to_liters,
    12: liters_to_pints
}


def elector(number):
    directory_of_functions[number]()


def converter():
    while True:
        n = int(input("Input number between 1 and 12 for choose a function: "))
        if 1 <= n <= 12:
            elector(n)
        elif n == 0:
            break
        else:
            continue


[print(key, value) for key, value in directory_of_functions.items()]
print("\nPress 0 for exit\n")
try:
    converter()
except ValueError:
    converter()
finally:
    print("Debiloidus! Read direction!")
    converter()
