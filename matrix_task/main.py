from matrix_task.create_matrix import create_matrix
from matrix_task.show_matrix import show_matrix
from matrix_task.sum_matrix import sum_matrix, sum_matrix_new
from matrix_task.search_matrix import search_matrix

matrix = create_matrix()
show_matrix(matrix)
sum_1 = sum_matrix(matrix)
sum_2 = sum_matrix_new(matrix)
print(sum_1, sum_2)
max_elem = search_matrix(matrix)
min_elem = search_matrix(matrix, mx=False)
print(max_elem, min_elem)
