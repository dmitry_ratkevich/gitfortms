def search_matrix(matrix, mx=True):
    result = None
    for row in matrix:
        for item in row:
            if result is None:
                result = item
            elif mx is True and item > result:
                result = item
            elif mx is False and item < result:
                result = item
    return result
