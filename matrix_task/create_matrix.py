from random import randint


def create_matrix():
    n = int(input('Input value for n:\n'))
    m = int(input('Input value for m:\n'))
    matrix = []
    for i in range(n):
        matrix.append([])
        for j in range(m):
            matrix[i].append(randint(1, 9))
    return matrix
