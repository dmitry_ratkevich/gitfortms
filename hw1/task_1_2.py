from random import randint


x = randint(-10, 10)
y = randint(-10, 10)
f = (abs(x)-abs(y))/(1+abs(x*y))
print(f'x={x}\ny={y}\nEquality=', "%.4f" % f)
