from random import randint
from math import sqrt

k1 = randint(2, 9)
k2 = randint(2, 9)
g = sqrt((k1**2)+(k2**2))
s = (k1*k2)/2
print(f'Legs {k1} and {k2}.\nHypotenuse = {g}\nSquare = {s}')
