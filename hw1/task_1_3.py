from random import randint


l = randint(1, 9)
s = l*l
v = l**3
print(f'Cube edge length = {l}\nSquare of one face = {s}\nVolume = {v}')
