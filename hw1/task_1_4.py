from random import randint


a = randint(1, 99)
b = randint(1, 99)
sar = (a+b)/2
sgeo = (a*b)/2
print(f'arithmetic mean {a} and {b} = {sar}\ngeometric mean {a} and {b} = {sgeo}')
