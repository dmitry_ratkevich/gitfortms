while True:
    v = input('Введите число:\n')
    try:
        w = int(v)
        if w >= 1000 and w % 1000 == 0:
            print('millennium')
            break
        else:
            print('Число не делится на 1000 без остатка.')
            break
    except:
        if v.isalpha():
            print('Вы ввели буквы, а нам нужно число.')
        else:
            print('Ошибка ввода!')
