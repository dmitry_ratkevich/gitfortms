def fact2(*args):
    for n in args:
        lst_n = [i for i in range(1, n + 1)]
        if n % 2 == 0:
            fact_a = list(filter(lambda x: x % 2 == 0, lst_n))
            s = 1
            for i in fact_a:
                s *= i
            print(s)
        elif n % 2 != 0:
            fact_b = list(filter(lambda x: x % 2 != 0, lst_n))
            s = 1
            for i in fact_b:
                s *= i
            print(s)
        else:
            print("Invalid arguments!")


fact2(0, 8, 9, 12, 17)
